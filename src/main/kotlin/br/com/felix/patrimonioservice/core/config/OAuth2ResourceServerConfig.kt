package br.com.felix.patrimonioservice.core.config

import org.springframework.context.annotation.Configuration
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity
import org.springframework.security.config.annotation.web.builders.HttpSecurity
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter

@Configuration
@EnableResourceServer //@EnableOAuth2Client
@EnableGlobalMethodSecurity(prePostEnabled = true)
class OAuth2ResourceServerConfig : ResourceServerConfigurerAdapter() {


    override fun configure(http: HttpSecurity) {
        http.authorizeRequests()
                .antMatchers("/actuator/**", "/solicitacaomateriais/**").permitAll()
                .anyRequest().authenticated()
    }


}