package br.com.felix.patrimonioservice.core.crud

import org.springframework.data.jpa.repository.JpaRepository
import java.io.Serializable

abstract class CrudServiceImpl<T, ID : Serializable> : CrudService<T, ID> {

    abstract fun getRepository() : JpaRepository<T, ID>

    override fun findById(id: ID): T? = getRepository().findById(id).orElse(null)

    override fun findAll(): List<T>? = getRepository().findAll()

    override fun save(entity: T): T = getRepository().save(entity)

    override fun saveAll(entities: List<T>): List<T> = getRepository().saveAll(entities)

    override fun delete(entity: T) = getRepository().delete(entity)

    override fun delete(id: ID) = getRepository().deleteById(id)

}
