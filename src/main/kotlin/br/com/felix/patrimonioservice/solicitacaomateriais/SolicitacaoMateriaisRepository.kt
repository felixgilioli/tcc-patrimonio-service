package br.com.felix.patrimonioservice.solicitacaomateriais

import org.springframework.data.jpa.repository.JpaRepository

interface SolicitacaoMateriaisRepository : JpaRepository<SolicitacaoMateriais, Long> {
}