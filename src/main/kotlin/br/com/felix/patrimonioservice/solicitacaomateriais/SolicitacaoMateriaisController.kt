package br.com.felix.patrimonioservice.solicitacaomateriais

import br.com.felix.patrimonioservice.core.crud.CrudController
import org.springframework.kafka.core.KafkaTemplate
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("solicitacaomateriais")
class SolicitacaoMateriaisController(private val service: SolicitacaoMateriaisService,
                                     private val kafkaTemplate: KafkaTemplate<String, String>) : CrudController<SolicitacaoMateriais, Long>() {

    override fun getService() = service

    @GetMapping("teste/{message}")
    fun teste(@PathVariable message: String) {
        kafkaTemplate.send("test", message)
    }

}