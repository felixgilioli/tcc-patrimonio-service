package br.com.felix.patrimonioservice.solicitacaomateriais

import org.hibernate.validator.constraints.Length
import java.time.LocalDate
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
data class SolicitacaoMateriais(

        @NotNull(message = "igrejaSolicitanteId nao pode ser nula.")
        @Column(name = "igreja_solicitante", nullable = false)
        val igrejaSolicitanteId: Long,

        @Length(min = 2, max = 500,  message = "mensagem deve estar entre 2 e 500 caracteres.")
        @Column(length = 500, nullable = false)
        val mensagem: String,

        @NotNull(message = "usuarioSolicitanteId nao pode ser nulo.")
        @Column(name = "usuario_solicitante", nullable = false)
        val usuarioSolicitanteId: Long,

        @NotNull(message = "dataSolicitacao nao pode ser nula.")
        @Column(name = "data_solicitacao", nullable = false)
        val dataSolicitacao: LocalDate = LocalDate.now(),

        @NotNull(message = "dataPrevista nao pode ser nula.")
        @Column(name = "data_prevista", nullable = false)
        val dataPrevista: LocalDate = LocalDate.now(),

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idsolicitacao_materiais")
        val idSolicitacaoMateriais: Long? = null
)