package br.com.felix.patrimonioservice.solicitacaomateriais

import br.com.felix.patrimonioservice.core.crud.CrudService

interface SolicitacaoMateriaisService : CrudService<SolicitacaoMateriais, Long> {
}