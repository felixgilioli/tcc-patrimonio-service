package br.com.felix.patrimonioservice.solicitacaomateriais

import br.com.felix.patrimonioservice.core.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class SolicitacaoMateriaisServiceImpl(private val repository: SolicitacaoMateriaisRepository)
    : SolicitacaoMateriaisService, CrudServiceImpl<SolicitacaoMateriais, Long>() {

    override fun getRepository() = repository
}