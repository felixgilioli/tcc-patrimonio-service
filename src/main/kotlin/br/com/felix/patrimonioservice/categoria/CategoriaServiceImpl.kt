package br.com.felix.patrimonioservice.categoria

import br.com.felix.patrimonioservice.core.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class CategoriaServiceImpl(private val repository: CategoriaRepository) : CategoriaService, CrudServiceImpl<Categoria, Long>() {

    override fun getRepository() = repository
}