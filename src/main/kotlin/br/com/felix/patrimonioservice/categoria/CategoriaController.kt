package br.com.felix.patrimonioservice.categoria

import br.com.felix.patrimonioservice.core.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("categoria")
class CategoriaController(private val service: CategoriaService) : CrudController<Categoria, Long>() {

    override fun getService() = service
}