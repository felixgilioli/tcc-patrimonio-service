package br.com.felix.patrimonioservice.categoria

import br.com.felix.patrimonioservice.core.crud.CrudService

interface CategoriaService : CrudService<Categoria, Long> {
}