package br.com.felix.patrimonioservice.categoria

import org.hibernate.validator.constraints.Length
import javax.persistence.*

@Entity
data class Categoria(

        @Length(min = 2, max = 100,  message = "nome deve estar entre 2 e 100 caracteres.")
        @Column(length = 100, nullable = false)
        val nome: String,

        @Length(max = 255,  message = "descricao nao pode ultrapassar 255 caracteres.")
        @Column(length = 255)
        val descricao: String? = null,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idcategoria")
        val idCategoria: Long? = null
)