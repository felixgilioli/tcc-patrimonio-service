package br.com.felix.patrimonioservice.solicitacaomateriaispatrimonio

import br.com.felix.patrimonioservice.core.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class SolicitacaoMateriaisPatrimonioServiceImpl(private val repository: SolicitacaoMateriaisPatrimonioRepository)
    : SolicitacaoMateriaisPatrimonioService, CrudServiceImpl<SolicitacaoMateriaisPatrimonio, Long>() {

    override fun getRepository() = repository
}