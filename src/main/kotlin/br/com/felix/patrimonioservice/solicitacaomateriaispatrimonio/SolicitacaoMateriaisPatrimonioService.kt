package br.com.felix.patrimonioservice.solicitacaomateriaispatrimonio

import br.com.felix.patrimonioservice.core.crud.CrudService

interface SolicitacaoMateriaisPatrimonioService : CrudService<SolicitacaoMateriaisPatrimonio, Long> {
}