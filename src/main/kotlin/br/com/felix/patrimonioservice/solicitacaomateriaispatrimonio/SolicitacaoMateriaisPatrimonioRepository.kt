package br.com.felix.patrimonioservice.solicitacaomateriaispatrimonio

import org.springframework.data.jpa.repository.JpaRepository

interface SolicitacaoMateriaisPatrimonioRepository : JpaRepository<SolicitacaoMateriaisPatrimonio, Long> {
}