package br.com.felix.patrimonioservice.solicitacaomateriaispatrimonio

import br.com.felix.patrimonioservice.patrimonio.Patrimonio
import br.com.felix.patrimonioservice.solicitacaomateriais.SolicitacaoMateriais
import org.hibernate.validator.constraints.Length
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(name = "solicitacao_materiais_patrimonio")
data class SolicitacaoMateriaisPatrimonio(

        @Length(max = 500,  message = "observacao nao pode ultrapassar 500 caracteres.")
        @Column(length = 500)
        val observacao: String? = null,

        @NotNull(message = "solicitacaoMateriais nao pode ser nulo.")
        @ManyToOne(optional = false)
        @JoinColumn(name = "solicitacao_materiais_idsolicitacao_materiais", nullable = false)
        val solicitacaoMateriais: SolicitacaoMateriais,

        @NotNull(message = "patrimonio nao pode ser nulo.")
        @ManyToOne(optional = false)
        @JoinColumn(name = "patrimonio_idpatrimonio", nullable = false)
        val patrimonio: Patrimonio,

        @NotNull(message = "quantidade nao pode ser nula.")
        @Column(nullable = false)
        val quantidade: Int,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idsolicitacao_materiais_patrimonio")
        val idSolicitacaoMateriaisPatrimonio: Long? = null
)