package br.com.felix.patrimonioservice.patrimonio

import br.com.felix.patrimonioservice.categoria.Categoria
import org.hibernate.validator.constraints.Length
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
data class Patrimonio(

        @Length(min = 2, max = 100,  message = "nome deve estar entre 2 e 100 caracteres.")
        @Column(length = 100, nullable = false)
        val nome: String,

        @Length(max = 255,  message = "descricao nao pode ultrapassar 255 caracteres.")
        @Column(length = 255)
        val descricao: String? = null,

        @NotNull(message = "quantidade nao pode ser nula.")
        @Column(nullable = false)
        val quantidade: Int,

        @Length(max = 45, message = "codigo nao pode ultrassar 45 caracteres.")
        @Column(length = 45)
        val codigo: String? = null,

        @NotNull(message = "categoria nao pode ser nula.")
        @ManyToOne(optional = false)
        @JoinColumn(name = "categoria_idcategoria", nullable = false)
        val categoria: Categoria,

        @NotNull(message = "igrejaId nao pode ser nula.")
        @Column(name = "igreja_id", nullable = false)
        val igrejaId: Long,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idpatrimonio")
        val idCategoria: Long? = null
)