package br.com.felix.patrimonioservice.patrimonio

import org.springframework.data.jpa.repository.JpaRepository

interface PatrimonioRepository : JpaRepository<Patrimonio, Long> {
}