package br.com.felix.patrimonioservice.patrimonio

import br.com.felix.patrimonioservice.core.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class PatrimonioServiceImpl(private val repository: PatrimonioRepository) : PatrimonioService, CrudServiceImpl<Patrimonio, Long>() {

    override fun getRepository() = repository
}