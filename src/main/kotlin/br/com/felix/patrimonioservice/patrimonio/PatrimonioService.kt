package br.com.felix.patrimonioservice.patrimonio

import br.com.felix.patrimonioservice.core.crud.CrudService

interface PatrimonioService : CrudService<Patrimonio, Long> {
}