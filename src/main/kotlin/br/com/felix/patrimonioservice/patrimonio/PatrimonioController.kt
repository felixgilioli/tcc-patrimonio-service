package br.com.felix.patrimonioservice.patrimonio

import br.com.felix.patrimonioservice.core.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("patrimonio")
class PatrimonioController(private val service: PatrimonioService) : CrudController<Patrimonio, Long>() {

    override fun getService() = service
}