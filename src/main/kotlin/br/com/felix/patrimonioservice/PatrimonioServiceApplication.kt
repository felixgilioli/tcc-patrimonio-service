package br.com.felix.patrimonioservice

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication
import org.springframework.cloud.netflix.eureka.EnableEurekaClient

@EnableEurekaClient
@SpringBootApplication
class PatrimonioServiceApplication

fun main(args: Array<String>) {
	runApplication<PatrimonioServiceApplication>(*args)
}

