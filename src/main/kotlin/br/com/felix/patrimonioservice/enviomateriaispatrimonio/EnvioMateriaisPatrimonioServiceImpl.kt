package br.com.felix.patrimonioservice.enviomateriaispatrimonio

import br.com.felix.patrimonioservice.core.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class EnvioMateriaisPatrimonioServiceImpl(private val repository: EnvioMateriaisPatrimonioRepository)
    : EnvioMateriaisPatrimonioService, CrudServiceImpl<EnvioMateriaisPatrimonio, Long>() {

    override fun getRepository() = repository
}