package br.com.felix.patrimonioservice.enviomateriaispatrimonio

import org.springframework.data.jpa.repository.JpaRepository

interface EnvioMateriaisPatrimonioRepository : JpaRepository<EnvioMateriaisPatrimonio, Long> {
}