package br.com.felix.patrimonioservice.enviomateriaispatrimonio

import br.com.felix.patrimonioservice.enviomateriais.EnvioMateriais
import br.com.felix.patrimonioservice.solicitacaomateriais.SolicitacaoMateriais
import org.hibernate.validator.constraints.Length
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(name = "envio_materiais_patrimonio")
data class EnvioMateriaisPatrimonio(

        @NotNull(message = "solicitacaoMateriais nao pode ser nulo.")
        @ManyToOne(optional = false)
        @JoinColumn(name = "solicitacao_materiais_id", nullable = false)
        val solicitacaoMateriais: SolicitacaoMateriais,

        @NotNull(message = "quantidade nao pode ser nula.")
        @Column(nullable = false)
        val quantidade: Int,

        @Length(max = 500,  message = "observacao nao pode ultrapassar 500 caracteres.")
        @Column(length = 500)
        val observacao: String? = null,

        @NotNull(message = "envioMateriais nao pode ser nulo.")
        @ManyToOne(optional = false)
        @JoinColumn(name = "envio_materiais_idenvio_materiais", nullable = false)
        val envioMateriais: EnvioMateriais,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idenvio_materiais_patrimonio")
        val idEnvioMateriaisPatrimonio: Long? = null
)