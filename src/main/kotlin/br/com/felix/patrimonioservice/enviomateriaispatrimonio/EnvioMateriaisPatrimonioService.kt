package br.com.felix.patrimonioservice.enviomateriaispatrimonio

import br.com.felix.patrimonioservice.core.crud.CrudService

interface EnvioMateriaisPatrimonioService : CrudService<EnvioMateriaisPatrimonio, Long> {
}