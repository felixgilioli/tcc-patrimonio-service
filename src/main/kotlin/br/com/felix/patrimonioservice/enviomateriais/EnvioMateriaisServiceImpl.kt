package br.com.felix.patrimonioservice.enviomateriais

import br.com.felix.patrimonioservice.core.crud.CrudServiceImpl
import org.springframework.stereotype.Service

@Service
class EnvioMateriaisServiceImpl(private val repository: EnvioMateriaisRepository) : EnvioMateriaisService, CrudServiceImpl<EnvioMateriais, Long>() {

    override fun getRepository() = repository
}