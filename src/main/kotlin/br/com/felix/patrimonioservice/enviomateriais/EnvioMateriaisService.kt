package br.com.felix.patrimonioservice.enviomateriais

import br.com.felix.patrimonioservice.core.crud.CrudService

interface EnvioMateriaisService : CrudService<EnvioMateriais, Long> {
}