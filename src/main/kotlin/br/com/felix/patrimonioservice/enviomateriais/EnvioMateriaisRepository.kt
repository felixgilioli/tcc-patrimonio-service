package br.com.felix.patrimonioservice.enviomateriais

import org.springframework.data.jpa.repository.JpaRepository

interface EnvioMateriaisRepository : JpaRepository<EnvioMateriais, Long> {
}