package br.com.felix.patrimonioservice.enviomateriais

import br.com.felix.patrimonioservice.solicitacaomateriais.SolicitacaoMateriais
import org.hibernate.validator.constraints.Length
import java.time.LocalDate
import javax.persistence.*
import javax.validation.constraints.NotNull

@Entity
@Table(name = "envio_materiais")
data class EnvioMateriais(

        @NotNull(message = "dataEnvio nao pode ser nula.")
        @Column(name = "data_envio", nullable = false)
        val dataEnvio: LocalDate = LocalDate.now(),

        @Length(max = 500,  message = "observacao nao pode ultrapassar 500 caracteres.")
        @Column(length = 500)
        val observacao: String? = null,

        @NotNull(message = "solicitacaoMateriais nao pode ser nula.")
        @ManyToOne(optional = false)
        @JoinColumn(name = "solicitacao_materiais_id", nullable = false)
        val solicitacaoMateriais: SolicitacaoMateriais,

        @Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
        @Column(name = "idenvio_materiais")
        val idEnvioMateriais: Long? = null
)