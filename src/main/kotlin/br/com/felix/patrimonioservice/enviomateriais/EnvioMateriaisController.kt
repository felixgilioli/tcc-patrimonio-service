package br.com.felix.patrimonioservice.enviomateriais

import br.com.felix.patrimonioservice.core.crud.CrudController
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("enviomateriais")
class EnvioMateriaisController(private val service: EnvioMateriaisService) : CrudController<EnvioMateriais, Long>() {

    override fun getService() = service
}