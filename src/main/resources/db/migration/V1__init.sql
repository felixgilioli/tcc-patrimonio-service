-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema patrimonio
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Schema patrimonio
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `patrimonio` DEFAULT CHARACTER SET utf8 ;
USE `patrimonio` ;

-- -----------------------------------------------------
-- Table `patrimonio`.`categoria`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `patrimonio`.`categoria` (
  `idcategoria` BIGINT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `descricao` VARCHAR(255) NULL,
  PRIMARY KEY (`idcategoria`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `patrimonio`.`patrimonio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `patrimonio`.`patrimonio` (
  `idpatrimonio` BIGINT NOT NULL AUTO_INCREMENT,
  `nome` VARCHAR(100) NOT NULL,
  `descricao` VARCHAR(255) NULL,
  `quantidade` INT NOT NULL,
  `codigo` VARCHAR(45) NULL,
  `categoria_idcategoria` BIGINT NOT NULL,
  `igreja_id` BIGINT NOT NULL,
  PRIMARY KEY (`idpatrimonio`),
  INDEX `fk_patrimonio_categoria1_idx` (`categoria_idcategoria` ASC),
  CONSTRAINT `fk_patrimonio_categoria1`
    FOREIGN KEY (`categoria_idcategoria`)
    REFERENCES `patrimonio`.`categoria` (`idcategoria`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `patrimonio`.`solicitacao_materiais`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `patrimonio`.`solicitacao_materiais` (
  `idsolicitacao_materiais` BIGINT NOT NULL AUTO_INCREMENT,
  `igreja_solicitante` BIGINT NOT NULL,
  `mensagem` VARCHAR(500) NOT NULL,
  `usuario_solicitante` BIGINT NOT NULL,
  `data_solicitacao` DATE NOT NULL,
  `data_prevista` DATE NULL,
  PRIMARY KEY (`idsolicitacao_materiais`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `patrimonio`.`solicitacao_materiais_patrimonio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `patrimonio`.`solicitacao_materiais_patrimonio` (
  `idsolicitacao_materiais_patrimonio` BIGINT NOT NULL AUTO_INCREMENT,
  `observacao` VARCHAR(500) NULL,
  `solicitacao_materiais_idsolicitacao_materiais` BIGINT NOT NULL,
  `patrimonio_idpatrimonio` BIGINT NOT NULL,
  `quantidade` INT NOT NULL,
  PRIMARY KEY (`idsolicitacao_materiais_patrimonio`),
  INDEX `fk_solicitacao_materiais_patrimonio_solicitacao_materiais1_idx` (`solicitacao_materiais_idsolicitacao_materiais` ASC),
  INDEX `fk_solicitacao_materiais_patrimonio_patrimonio1_idx` (`patrimonio_idpatrimonio` ASC),
  CONSTRAINT `fk_solicitacao_materiais_patrimonio_solicitacao_materiais1`
    FOREIGN KEY (`solicitacao_materiais_idsolicitacao_materiais`)
    REFERENCES `patrimonio`.`solicitacao_materiais` (`idsolicitacao_materiais`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_solicitacao_materiais_patrimonio_patrimonio1`
    FOREIGN KEY (`patrimonio_idpatrimonio`)
    REFERENCES `patrimonio`.`patrimonio` (`idpatrimonio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `patrimonio`.`envio_materiais`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `patrimonio`.`envio_materiais` (
  `idenvio_materiais` BIGINT NOT NULL AUTO_INCREMENT,
  `data_envio` DATE NOT NULL,
  `observacao` VARCHAR(500) NULL,
  `solicitacao_materiais_id` BIGINT NOT NULL,
  PRIMARY KEY (`idenvio_materiais`),
  INDEX `fk_envio_materiais_solicitacao_materiais1_idx` (`solicitacao_materiais_id` ASC),
  CONSTRAINT `fk_envio_materiais_solicitacao_materiais1`
    FOREIGN KEY (`solicitacao_materiais_id`)
    REFERENCES `patrimonio`.`solicitacao_materiais` (`idsolicitacao_materiais`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `patrimonio`.`envio_materiais_patrimonio`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `patrimonio`.`envio_materiais_patrimonio` (
  `idenvio_materiais_patrimonio` BIGINT NOT NULL AUTO_INCREMENT,
  `solicitacao_materiais_id` BIGINT NOT NULL,
  `quantidade` INT NOT NULL,
  `observacao` VARCHAR(500) NULL,
  `envio_materiais_idenvio_materiais` BIGINT NOT NULL,
  PRIMARY KEY (`idenvio_materiais_patrimonio`),
  INDEX `fk_envio_materiais_patrimonio_solicitacao_materiais_patrimo_idx` (`solicitacao_materiais_id` ASC),
  INDEX `fk_envio_materiais_patrimonio_envio_materiais1_idx` (`envio_materiais_idenvio_materiais` ASC),
  CONSTRAINT `fk_envio_materiais_patrimonio_solicitacao_materiais_patrimonio1`
    FOREIGN KEY (`solicitacao_materiais_id`)
    REFERENCES `patrimonio`.`solicitacao_materiais_patrimonio` (`idsolicitacao_materiais_patrimonio`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_envio_materiais_patrimonio_envio_materiais1`
    FOREIGN KEY (`envio_materiais_idenvio_materiais`)
    REFERENCES `patrimonio`.`envio_materiais` (`idenvio_materiais`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
