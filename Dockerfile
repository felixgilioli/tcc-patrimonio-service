FROM openjdk:11-jdk-slim
ENV APP_HOME=/usr/app/
WORKDIR $APP_HOME
COPY ./build/libs/* ./patrimonio-service-0.0.1-SNAPSHOT.jar
EXPOSE 8888
CMD ["java","-jar","patrimonio-service-0.0.1-SNAPSHOT.jar"]